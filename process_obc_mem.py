import os
import json
import re

experiment_path = "experiments_data/suchai3_obc_exp2_8_3/original/"
folders = os.listdir(experiment_path)

for folder in folders:
    if folder[0:6] == "strat_":
        logs_path = experiment_path + folder + "/logs/"
        sent_seqs_path = experiment_path + folder + "/sent_sequences/"
        log_files = os.listdir(logs_path)
        for log in log_files:
            seq_mem = []
            with open(logs_path + log, "r") as logfile:
                mem_patterns = ["^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] Total non-mmapped bytes \(arena\): +[0-9]*",
                                "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] \# of free chunks \(ordblks\): +[0-9]*",
                                "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] \# of free fastbin blocks \(smblks\): +[0-9]*",
                                "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] \# of mapped regions \(hblks\): +[0-9]*",
                                "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] Bytes in mapped regions \(hblkhd\): +[0-9]*",
                                "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] Max\. total allocated space \(usmblks\): +[0-9]*",
                                "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] Free bytes held in fastbins \(fsmblks\): +[0-9]*",
                                "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] Total allocated space \(uordblks\): +[0-9]*",
                                "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] Total free space \(fordblks\): +[0-9]*",
                                "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] Topmost releasable block \(keepcost\): +[0-9]*"]
                log_str = logfile.read()
            log_list = log_str.split('\n')
            for line in log_list:
                for pattern in mem_patterns:
                    if re.fullmatch(pattern, line):
                        # Key = Description of variable, Value
                        seq_mem.append({line.split(":")[0].split("] ")[1]: int(line.split(" ")[-1])})

            # Process memory
            with open(sent_seqs_path + log, "r") as json_file:
                seq_dic = json.load(json_file)
            with open(sent_seqs_path + log, "w") as json_file:
                seq_dic["mem_ini"] = seq_mem[0:10]
                if len(seq_mem) == 20:
                    seq_dic["mem_fin"] = seq_mem[10:20]
                json.dump(seq_dic, json_file, indent=2, separators=(',', ': '))