def run_experiment(random_fuzzer, iterations=10, cmds_number=10):
    """
    Create a random fuzzer instance to execute flight software with random input.
    :param random_fuzzer: Class. Fuzzer.
    :param iterations: Int.
    :param cmds_number: Int. Number of commands to execute each iteration.
    :return:
    """
    print("Commands number: " + str(cmds_number) + ", iteration: " + str(iterations))
    outcomes = random_fuzzer.generate_seqs(iterations)

    return outcomes


def fuzz(iter, commands_number, min_length, max_length, char_start, char_range, fuzz_class, commands_file):
    """
    :param iter: Int. Number of sequences.
    :param commands_number: Int. Each element represents a commands' number in a sequence.
    :param min_length: Int. Minimum length of the random command names.
    :param max_length: Int. Maximum length of the random command names.
    :param char_start: Int. Index of the range that indicates where to start producing random command names in ASCII code.
    :param char_range: Int. Length of the characters range in ASCII code.
    :param fuzz_class: Class. Fuzzer class to be used.
    :param commands_file: String. Filename with the SUCHAI Flight Software commands and parameters type.
    :return:
    """
    # Run experiment
    # Create fuzzer instance
    fuzzer = fuzz_class(commands_file, min_length=min_length, max_length=max_length, char_start=char_start, char_range=char_range, n_cmds=commands_number)

    input_list = run_experiment(fuzzer, int(iter), int(commands_number))
    return input_list

