from os import listdir
import matplotlib.pyplot as plt
import json
import numpy as np
from pathlib import Path

# Process specific experiment data. Iterates over the three strategies.

exp_name = "suchai3_obc_exp2_8_3/original/"  # Must exclude initial "/" character and include last "/" character
strats = ["strat_1", "strat_2", "strat_3"]

for strat in strats:
    path = "experiments_data/" + exp_name + strat + "/sent_sequences/"
    files = [f for f in listdir(path)]
    files.sort()
    python_exec_time = []
    obc_exec_time = []
    exceeded_namexy_tuples = []

    x = 1
    for file in files:
        with open(path+file, 'r+') as filepath:
            sequence = json.load(filepath)
            python_exec_time.append(sequence["python_exec_time"])
            if "obc_exec_time" in sequence.keys():
                obc_exec_time.append(sequence["obc_exec_time"])
            if sequence["python_exec_time"] > 50:
                exceeded_namexy_tuples.append((file,x,sequence["python_exec_time"]))
            elif "obc_exec_time" in sequence.keys() and sequence["obc_exec_time"] > 50:
                exceeded_namexy_tuples.append((file,x,sequence["obc_exec_time"]))
            else:
                pass
        x+=1

    strat_number = strat.split("_")[-1]
    plt.title("Execution time of each sequence - Strategy " + strat_number)
    plt.plot(python_exec_time, '-o', color='red', label="Execution time measured with time.time()")
    plt.plot(obc_exec_time, '-o', color='blue', label="Exec. time measured with obc_get_time command")
    plt.xlabel("Sequence index")
    plt.ylabel("Execution time (s)")
    plt.legend(loc="upper left")
    plt.yscale("log")
    plt.ylim(top=1400, bottom=0.5)
    plt.xticks(np.arange(0, 8, step=4), fontsize=9)
    plt.grid("on")

    for tuple in exceeded_namexy_tuples:
        plt.annotate(tuple[0], xy=(tuple[1], tuple[2]))

    final_dest = "experiments_data/" + exp_name + "plots/"
    path = Path(final_dest)
    path.mkdir(parents=True, exist_ok=True)
    figure_name = exp_name.split("/")[0] + "_" + strat + ".png"
    plt.savefig(final_dest + figure_name)
    plt.show()
