from os import listdir
import matplotlib.pyplot as plt
import json
import numpy as np
from pathlib import Path
import math

# Process specific experiment data. Iterates over the three strategies.

exp_name = "stt_exp1_500_5/original/"  # Must exclude initial "/" character and include last "/" character
strats = ["strat_1", "strat_2", "strat_3"]

# Process min and max of all strategies for each variable
mem_mins = [math.inf] * 10
mem_maxs = [-math.inf] * 10

for strat in strats:
    path = "experiments_data/" + exp_name + strat + "/sent_sequences/"
    files = [f for f in listdir(path)]
    files.sort()
    mem_keys = ["Total non-mmapped bytes (arena)", "# of free chunks (ordblks)", "# of free fastbin blocks (smblks)",
               "# of mapped regions (hblks)", "Bytes in mapped regions (hblkhd)",
               "Max. total allocated space (usmblks)", "Free bytes held in fastbins (fsmblks)",
               "Total allocated space (uordblks)", "Total free space (fordblks)", "Topmost releasable block (keepcost)"]

    for file in files:
        with open(path+file, 'r+') as filepath:
            sequence = json.load(filepath)

            # Check if mem_ini was written in the report
            if "mem_ini" in sequence.keys():
                mem_ini_list = sequence["mem_ini"]
                for key in mem_keys:
                    key_ind = mem_keys.index(key)
                    mem_mins[key_ind] = min(mem_mins[key_ind], list(mem_ini_list[key_ind].values())[0])

            # Check if mem_ini was written in the report
            if "mem_fin" in sequence.keys():
                mem_fin_list = sequence["mem_fin"]
                for key in mem_keys:
                    key_ind = mem_keys.index(key)
                    mem_maxs[key_ind] = max(mem_maxs[key_ind], list(mem_fin_list[key_ind].values())[0])

# Process data and plot
for strat in strats:
    path = "experiments_data/" + exp_name + strat + "/sent_sequences/"
    files = [f for f in listdir(path)]
    files.sort()
    seqs_number = len(files)
    print("seqs number", seqs_number)
    mem_ini = {}
    mem_fin = {}
    mem_keys = ["Total non-mmapped bytes (arena)", "# of free chunks (ordblks)", "# of free fastbin blocks (smblks)",
               "# of mapped regions (hblks)", "Bytes in mapped regions (hblkhd)",
               "Max. total allocated space (usmblks)", "Free bytes held in fastbins (fsmblks)",
               "Total allocated space (uordblks)", "Total free space (fordblks)", "Topmost releasable block (keepcost)"]
    mem_units = ["Bytes", "Ordinary free blocks", "Free fastbin blocks", "Mmapped blocks", "Bytes", "Bytes", "Bytes",
                 "Bytes", "Bytes", "Bytes"]

    for key in mem_keys:
        mem_ini[key] = []
        mem_fin[key] = []

    for file in files:
        with open(path+file, 'r+') as filepath:
            sequence = json.load(filepath)

            # Check if mem_ini was written in the report
            if "mem_ini" in sequence.keys():
                mem_ini_list = sequence["mem_ini"]
                for key in mem_keys:
                    key_ind = mem_keys.index(key)
                    mem_ini[key].append(list(mem_ini_list[key_ind].values())[0])
            else:
                for key in mem_keys:
                    key_ind = mem_keys.index(key)
                    mem_ini[key].append(math.nan)

            # Check if mem_ini was written in the report
            if "mem_fin" in sequence.keys():
                mem_fin_list = sequence["mem_fin"]
                for key in mem_keys:
                    key_ind = mem_keys.index(key)
                    mem_fin[key].append(list(mem_fin_list[key_ind].values())[0])
            else:
                for key in mem_keys:
                    key_ind = mem_keys.index(key)
                    mem_fin[key].append(math.nan)

    strat_number = strat.split("_")[-1]
    for key in mem_keys:
        plt.title(key + " per sequence - Strategy " + strat_number)
        plt.plot(mem_fin[key], '-o', color='red', label="Initial state", markersize=3, linewidth=3)
        plt.plot(mem_ini[key], '-o', color='blue', label="Final state", markersize=1)
        plt.xlabel("Sequence index")
        plt.ylabel(mem_units[mem_keys.index(key)])
        plt.legend(loc="upper left")
        plt.yscale("symlog")
        print("Strategy " + strat_number)
        # step = math.ceil(mem_maxs[mem_keys.index(key)]) - math.floor(mem_mins[mem_keys.index(key)]) + 2
        # number_of_y_ticks = 5
        # final_step = math.ceil(step)
        # top_y = math.floor(mem_mins[mem_keys.index(key)]) - 1 + number_of_y_ticks * final_step
        # plt.ylim(top=math.ceil(mem_maxs[mem_keys.index(key)]) + 1, bottom=math.floor(mem_mins[mem_keys.index(key)]) - 1)
        y_top = 1
        print(mem_maxs[mem_keys.index(key)])
        if mem_maxs[mem_keys.index(key)] != 0:
            y_top = 10**(math.ceil(math.log10(mem_maxs[mem_keys.index(key)]))) + 1
        plt.ylim(top=y_top, bottom=-1)
        plt.yticks()
        # plt.yticks(np.arange(math.floor(mem_mins[mem_keys.index(key)]) - 1, top_y, step=step))
        plt.xticks(np.arange(0, int(seqs_number), step=50))
        plt.grid("on")

        final_dest = "experiments_data/" + exp_name + "plots/"
        path = Path(final_dest)
        path.mkdir(parents=True, exist_ok=True)
        figure_name = exp_name.split("/")[0] + "_" + strat + "_" + key + ".png"
        plt.savefig(final_dest + figure_name)
        plt.show()
