#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#                          SERIAL COMMANDER
#                A simple serial interface to send commands
#
#      Copyright 2013-2021, Carlos Gonzalez Cortes, carlgonz@uchile.cl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
import time
import calendar
import shutil
import datetime
import threading
import serial
import json
import re
from serial.tools.list_ports import comports as listports
from pathlib import Path

from PyQt5.Qt import *
from PyQt5 import QtWidgets
from PyQt5 import QtCore

from forms.Console import Ui_MainWindow
from forms.EditCommandDialog import Ui_DialogEditCommandList
from forms.FuzzerDialog import Ui_Dialog

from run_experiment import fuzz
from randomsequencefuzzer import *
from randomsequencefuzzerwithfixedparams import *
from randomsequencefuzzerwithfixedparamsandexacttypes import *

# os.path.expanduser("C:\\Users\\$USERNAME\\AppData\\Roaming\\Python\\share\\serialcommander")]
current_path = os.path.dirname(os.path.abspath(__file__))
config_path = os.path.join(current_path, "../share/serialcommander")
local_config_path = os.path.join(os.path.expanduser("~"), ".config", "serialcommander")
print(config_path)

if not os.path.exists(local_config_path):
    print("No local config path. Creating")
    os.makedirs(local_config_path)

    if os.path.exists(config_path):
        shutil.copy(os.path.join(config_path, "config.json"), local_config_path)
        shutil.copy(os.path.join(config_path, "cmd_list.txt"), local_config_path)
        shutil.copy(os.path.join(config_path, "version.txt"), local_config_path)


class SerialCommander(QtWidgets.QMainWindow):
    """
    Clase principal, aca se crea y configura la ventana. Tambien contiene
    los slots y conexiones con otras senales
    """
    # Signals
    _new_char = pyqtSignal(type(""))  # Se lee un nuevo char
    fuzzingEnded = pyqtSignal(list, list, str)
    seqInfoCollected = pyqtSignal()

    def __init__(self):
        """
        Se inicializan la ventana llamando al metodo setupUi y se realizan
        las conexiones signals-slots
        """

        QtWidgets.QMainWindow.__init__(self)

        # Variables de instancia
        self.SerialComm = serial.Serial()
        self.alive = False
        self.receiver_thread = None
        self.commands_file = os.path.join(local_config_path, "cmd_list.txt")
        self.commands_list = []
        self.history = []
        self.history_cnt = 0
        self.history_max = 1000
        self.timestamp = False
        self.put_timestamp = True
        self.auto_scroll = True

        # Configurar ventana
        self.ventana = Ui_MainWindow()
        self.ventana.setupUi(self)
        self.setup_comm()
        self.setup_send()
        self.setup_actions()

        # Load version
        self.version = "0.0.0"
        try:
            with open(os.path.join(local_config_path, "version.txt")) as ver_file:
                self.version = ver_file.readline()
        except IOError:
            print("Version file not found")
            pass

        # Log
        self.log = []
        self.unidentified_fuzz_errors = []
        self.crashes_fuzz_errors = []

    def about(self):
        msg = "Version {}".format(self.version)
        QtWidgets.QMessageBox.information(self, "About", msg, QtWidgets.QMessageBox.Ok)
        return

    def setup_comm(self):
        """
        Configura el combo-box conexiones completando puertos
        y baudrates disponibles
        """

        # Conexiones
        self.ventana.pushButtonOpenPort.clicked.connect(self.open_port)
        self.ventana.pushButtonClosePort.clicked.connect(self.close_port)
        self.ventana.pushButtonPortRefresh.clicked.connect(self.refresh_port)

        # Se agregan los puertos disponibles
        self.refresh_port()

    def setup_send(self):
        """
        Configura la ventana de seleccion y envio de comandos
        """
        # Leer el archivo con la lista de comandos disponibles
        try:
            cmd_file = open(self.commands_file, 'r')
            self.commands_list = cmd_file.readlines()
            cmd_file.close()
        except OSError:
            cmd_file = open(self.commands_file, 'w')
            cmd_file.close()

        # Agregar los comandos a la lista
        for i in range(len(self.commands_list)):
            self.commands_list[i] = self.commands_list[i][:-1]

        self.ventana.listWidgetCommand.addItems(self.commands_list)

        # Conexiones
        self.ventana.listWidgetCommand.itemDoubleClicked.connect(self.command_clicked)
        self.ventana.pushButtonSend.clicked.connect(self.send_msg)
        self.ventana.checkBoxTimestamp.toggled.connect(self.timestamp_toggle)
        self.ventana.checkBoxScroll.toggled.connect(self.scroll_toggle)

    def timestamp_toggle(self, value):
        """
        Slot que intercambia entre agregar o no la marca de tiempo
        """
        self.timestamp = value

    def scroll_toggle(self, value):
        """
        Toggle auto scroll variable
        """
        self.auto_scroll = value

    def setup_actions(self):
        """
        Configura los menus de la barra de herramientas
        """
        self.ventana.actionGuardar.triggered.connect(self.save_log)
        self.ventana.actionAgregar_comando.triggered.connect(self.add_cmd)
        self.ventana.actionAcerca_de.triggered.connect(self.about)
        self.ventana.actionFuzz.triggered.connect(self.configure_fuzz)
        self._new_char.connect(self.write_terminal)
        #self._new_char.connect(self.show_sequence_log)
        self.fuzzingEnded.connect(self.show_fuzzing_errors)
        self.seqInfoCollected.connect(self.clear_window)

    def clear_window(self):
        self.ventana.textEditTerminal.clear()

    def add_cmd(self):
        """
        Edita la lista de comandos
        """
        dialog = EditCommandDialog(self, self.commands_list)
        self.commands_list = dialog.run_tool()

        self.ventana.listWidgetCommand.clear()
        self.ventana.listWidgetCommand.addItems(self.commands_list)

    def show_sequence_log(self, text):
        text = text.replace('\r', '')
        self.log.append(text)

    def write_terminal(self, text):
        """
        Escribe el texto en el widget de lectura, configura el
        cursor hasta el final de cocumento y agrega marca de tiempo
        """
        text = text.replace('\r', '')

        if self.timestamp:
            ts = "[{}] ".format(datetime.datetime.now().isoformat(' '))
            if self.put_timestamp:
                text = ts + text
            if text[-1] == "\n":
                self.put_timestamp = True
            text = text[:-1].replace("\n", "\n" + ts) + text[-1]

        prev_cursor = self.ventana.textEditTerminal.textCursor()
        prev_scrollbar_value = self.ventana.textEditTerminal.verticalScrollBar().value()

        self.ventana.textEditTerminal.moveCursor(QTextCursor.End)
        self.ventana.textEditTerminal.insertPlainText(text)
        self.ventana.textEditTerminal.moveCursor(QTextCursor.End)

        if not self.auto_scroll:
            self.ventana.textEditTerminal.setTextCursor(prev_cursor)
            self.ventana.textEditTerminal.verticalScrollBar().setValue(prev_scrollbar_value)

        self.log.append(text)

    def command_clicked(self, item):
        """
        Mueve un comando de la lista, a la salida de texto
        """
        self.ventana.lineEditSend.setText(item.text())

    def open_port(self):
        """
        Abre el puerto serial indicado en la GUI
        """
        self.ventana.pushButtonClosePort.setEnabled(True)
        self.ventana.pushButtonOpenPort.setEnabled(False)
        self.ventana.pushButtonSend.setEnabled(True)

        puerto = self.ventana.comboBoxPorts.currentText()
        baudrate = self.ventana.comboBoxBaudrate.currentText()

        self.SerialComm.port = str(puerto)
        self.SerialComm.baudrate = int(baudrate)
        self.SerialComm.timeout = 0.1  # [s]
        self.SerialComm.interCharTimeout = 0.01  # [s]

        try:
            self.SerialComm.open()
            self.alive = True
            self.receiver_thread = threading.Thread(target=self.reader)
            self.receiver_thread.daemon = True
            # self.receiver_thread.setDaemon(True)
            self.receiver_thread.start()
        except Exception as e:
            QMessageBox.critical(self, 'Error', 'Error al abrir el puerto serial\n' + str(e))
            self.close_port()

        print(self.SerialComm)

    def close_port(self):
        """
        Cierra la comunicacion serial actual
        """
        self.ventana.pushButtonClosePort.setEnabled(False)
        self.ventana.pushButtonOpenPort.setEnabled(True)
        # self.ventana.pushButtonSend.setEnabled(False)
        self.alive = False
        if self.receiver_thread is not None:
            self.receiver_thread.join()
        self.SerialComm.close()

    def refresh_port(self):
        """
        Refresh list of available ports and baudrates
        """
        # Add available ports
        comm_ports = listports()
        comm_ports.reverse()
        port_names = [port[0] for port in comm_ports]
        self.ventana.comboBoxPorts.clear()
        self.ventana.comboBoxPorts.addItems(port_names)

        # Add available baudrates
        baudrates = str(serial.Serial.BAUDRATES)[1:-1].split(',')
        self.ventana.comboBoxBaudrate.clear()
        self.ventana.comboBoxBaudrate.addItems(baudrates)

    def send_sequences(self, seqs, logs_dir):
        for seq in seqs:
            start_time = time.time()
            start_sequence_at = time.strftime('%d-%m-%Y_%H-%M-%S', time.localtime(start_time))
            #start_time_str = time.strptime(start_sequence_at, "%d-%m-%Y_%H-%M-%S")
            #start_time = calendar.timegm(start_time_str) - calendar.timegm(time.gmtime(0))
            print("Start time at: %d", start_time)

            self.send_sequence(seq)

            running = True
            while not self.write_reset_cmd():
                running = self.running()
                if not running:
                    self.send_cmd("obc_reset")
                    # break

            if running:
                while not self.ended_writing():
                    time.sleep(0.1)
            #end_time = calendar.timegm(time.time()) - calendar.timegm(time.gmtime(0))
            end_time = time.time()
            print("End time at: %d", end_time)

            python_exec_realtime = end_time - start_time
            self.write_sequence_vars(seq, logs_dir, start_sequence_at, python_exec_realtime)
            self.write_sequence_log(logs_dir, start_sequence_at)
            self.check_errors(start_sequence_at, running)  # This method considers when the app is hung
            time.sleep(0.5)
            self.seqInfoCollected.emit()
            time.sleep(0.5)
            self.log = []

            """ if not running:
                break """
            print("Sending sequence... ", seq)

    def running(self):
        running = True
        log_nlines = len(self.log)
        last_running_cmd = self.get_last_running_cmd()
        cnt = 0
        while len(self.log) == log_nlines:
            cnt += 1
            time.sleep(1)
            if cnt == 40:
                running = False
                break
        if running:
            while last_running_cmd == self.get_last_running_cmd():
                cnt += 1
                time.sleep(1)
                if cnt == 120:
                    running = False
                    break
        return running

    def get_last_running_cmd(self):
        cmd_exec_last_ind = -1
        for i, running_the_cmd in enumerate(self.log):
            if "Running the command: " in running_the_cmd:
                cmd_exec_last_ind = i
        return cmd_exec_last_ind

    def write_reset_cmd(self):
        log = ''.join(self.log)
        if "Running the command: obc_reset..." in log:
            return True
        return False

    def ended_writing(self):
        log = ''.join(self.log)
        if "[0;0mn] Started" in log:
            return True
        return False

    def write_sequence_vars(self, seq, logs_dir, name, exec_time):
        container_folder = 'sent_sequences'
        if logs_dir[-1] != '/':
            logs_dir += '/'

        final_destination = logs_dir + container_folder
        path = Path(final_destination)
        path.mkdir(parents=True, exist_ok=True)

        seq_dic = {}

        # Add list of commands
        seq_lst = []
        for cmd_params in seq:
            cmd_params_lst = cmd_params.split(" ", 1)
            cmd = cmd_params_lst[0]
            params = cmd_params_lst[1]
            seq_lst.append({"cmd_name": cmd, "params": params})
        seq_dic["cmds"] = seq_lst

        # Add time measurements
        seq_dic["python_exec_time"] = exec_time

        # Get and add time and memory measurements
        seq_mem = []
        seq_time = []
        log = ''.join(self.log)
        log_list = log.split('\n')

        time_pattern="^\[RES  \]\[[0-9]{1,10}\]\[repoData\] [0-9]{1,10}"

        mem_patterns = ["^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] Total non-mmapped bytes \(arena\): +[0-9]*",
                    "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] \# of free chunks \(ordblks\): +[0-9]*",
                    "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] \# of free fastbin blocks \(smblks\): +[0-9]*",
                    "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] \# of mapped regions \(hblks\): +[0-9]*",
                    "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] Bytes in mapped regions \(hblkhd\): +[0-9]*",
                    "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] Max\. total allocated space \(usmblks\): +[0-9]*",
                    "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] Free bytes held in fastbins \(fsmblks\): +[0-9]*",
                    "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] Total allocated space \(uordblks\): +[0-9]*",
                    "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] Total free space \(fordblks\): +[0-9]*",
                    "^\[RES  \]\[[0-9]{1,10}\]\[cmdOBC\] Topmost releasable block \(keepcost\): +[0-9]*"]

        for line in log_list:
            if re.fullmatch(time_pattern, line):
                seq_time.append(int(line.split("] ")[-1]))
            for pattern in mem_patterns:
                if re.fullmatch(pattern, line):
                    # Key = Description of variable, Value
                    seq_mem.append({line.split(":")[0].split("] ")[1]: int(line.split(" ")[-1])})

        # Process time
        if len(seq_time) >= 2:
            obc_start_time = seq_time[0]
            obc_end_time = seq_time[-1]
            obc_exec_time = obc_end_time - obc_start_time
            seq_dic["obc_exec_time"] = obc_exec_time

        # Process memory
        seq_dic["mem_ini"] = seq_mem[0:10]
        if len(seq_mem) == 20:
            seq_dic["mem_fin"] = seq_mem[10:20]

        with open(final_destination + "/" + name, "w") as outfile:
            json.dump(seq_dic, outfile, indent=2, separators=(',', ': '))

    def write_sequence_log(self, logs_dir, name):
        container_folder = 'logs'
        if logs_dir[-1] != '/':
            logs_dir += '/'

        final_destination = logs_dir + container_folder
        path = Path(final_destination)
        path.mkdir(parents=True, exist_ok=True)
        f = open(final_destination + "/" + name, "w")
        f.write(''.join(self.log))
        f.close()

    def check_errors(self, file_name, running):
        log = ''.join(self.log)
        log_list = log.split('\n')
        restarts_list = [line for line in log_list if "--------- FLIGHT SOFTWARE START ---------" in line]
        restarts = len(restarts_list)
        if restarts < 1 or not running:
            self.unidentified_fuzz_errors.append(file_name)
        elif restarts > 1:
            self.crashes_fuzz_errors.append(file_name)

    @QtCore.pyqtSlot(list, list, str)
    def show_fuzzing_errors(self, unidentified_fuzz_errors, crashes_fuzz_errors, logs_dir):
        msg = "No errors were found"
        if unidentified_fuzz_errors:
            msg = "Some unidentified errors have been found in files:\n"
            for filename in unidentified_fuzz_errors:
                msg += filename + "\n"
            msg += "\n"
        if crashes_fuzz_errors:
            msg = "Crashes have been found in files:\n"
            for filename in crashes_fuzz_errors:
                msg += filename + "\n"
        # report_starts_at = time.strftime('%d-%m-%Y_%H-%M-%S')

        if logs_dir[-1] != '/':
            logs_dir += '/'
        path = Path(logs_dir)
        path.mkdir(parents=True, exist_ok=True)
        with open(logs_dir + 'report.txt', 'w') as file:
            file.write(msg)

        QtWidgets.QMessageBox.information(self, "Found errors", msg, QtWidgets.QMessageBox.Ok)
        self.unidentified_fuzz_errors = []
        self.crashes_fuzz_errors = []
        return

    def send_sequence(self, seq):
        """
        Send sequence of commands using the serial port
        """
        if not self.alive:
            return

        # Clean database and register initial status (initial time, memory and eps status)
        self.send_cmd("obc_cancel_deploy")
        time.sleep(0.2)
        self.send_cmd("drp_reset_status 1010")
        time.sleep(0.2)
        self.send_cmd("gssb_pwr 1 1")  # COMMENT THIS IF IT'S NOT NECESSARY TO PUT THE RW ON
        time.sleep(0.2)  # COMMENT THIS IF IT'S NOT NECESSARY TO PUT THE RW ON
        self.send_cmd("eps_set_output 3 1") # COMMENT THIS IF IT'S NOT NECESSARY TO PUT THE RW ON
        time.sleep(0.2) # COMMENT THIS IF IT'S NOT NECESSARY TO PUT THE RW ON
        self.send_cmd("obc_get_time 1") # UNCOMMENT THIS IF IT'S NECESSARY TO MEASURE OBC TIME
        time.sleep(0.2) # UNCOMMENT THIS IF IT'S NECESSARY TO MEASURE OBC TIME
        self.send_cmd("obc_get_mem")
        time.sleep(0.2)
        self.send_cmd("eps_get_hk")
        time.sleep(0.2)

        for cmd in seq:
            self.send_cmd(cmd)

        # Register final status (final time, memory and eps status)
        self.send_cmd("eps_get_hk")
        time.sleep(0.2)
        self.send_cmd("obc_get_mem")
        time.sleep(0.2)
        self.send_cmd("obc_get_time 1") # UNCOMMENT THIS IF IT'S NECESSARY TO MEASURE OBC TIME
        time.sleep(0.2) # UNCOMMENT THIS IF IT'S NECESSARY TO MEASURE OBC TIME
        self.send_cmd("obc_reset")
        time.sleep(1)

    def send_cmd(self, cmd):
        # Add LF, CR and/or Echo
        if self.ventana.checkBoxEcho.isChecked():
            self._new_char.emit(cmd)
        if self.ventana.checkBoxLF.isChecked():
            cmd += '\n'
        if self.ventana.checkBoxCR.isChecked():
            cmd += '\r'

        self.SerialComm.write(cmd.encode("ASCII"))
        print(cmd)
        time.sleep(0.5)

    def send_msg(self):
        """
        Send text using the serial port
        """
        if not self.alive:
            self.ventana.lineEditSend.clear()
            return

        msg = str(self.ventana.lineEditSend.text())
        self.add_history(msg)

        # Add LF, CR and/or Echo
        if self.ventana.checkBoxEcho.isChecked():
            self._new_char.emit(msg)
        if self.ventana.checkBoxLF.isChecked():
            msg += '\n'
        if self.ventana.checkBoxCR.isChecked():
            msg += '\r'

        self.SerialComm.write(msg.encode("ASCII"))
        self.ventana.lineEditSend.clear()
        self.history_cnt = 0

    def save_log(self):
        """
        Guarda el contenido de la ventana de log a un archivo
        """
        # Get filename dialog
        doc_file, _ = QFileDialog.getSaveFileName(self, "Guardar archivo", QDir.currentPath(),
                                                  "Archivos de texto (*.txt);;All files (*.*)")
        if doc_file:
            doc_file = str(doc_file)
            # Save file using python
            with open(doc_file, 'w') as text_file:
                text_file.write(self.ventana.textEditTerminal.toPlainText())

    def reader(self):
        """
        Lee en un thread los datos seriales y los muestra en pantalla
        """
        while self.alive:
            try:
                to_read = self.SerialComm.inWaiting()
                if to_read:
                    data = self.SerialComm.read(to_read)
                    if len(data) and (not data == '\r'):
                        self._new_char.emit(data.decode("ASCII"))
                else:
                    time.sleep(0.1)

            except UnicodeDecodeError:
                pass

            except serial.SerialException:
                self.alive = False
                raise

    def add_history(self, line):
        """
        Agrega una nueva linea al historial. Elimina entradas antiguas si supera
        cierta cantidad de mensajes.
        """
        try:
            if not (line == self.history[-1]):
                self.history.append(line)
        except IndexError:
            self.history.append(line)

        if len(self.history) > self.history_max:
            self.history.pop(0)

    def get_history(self, index):
        """
        Retorna el elemendo numero index del historial
        """
        if 0 < index <= len(self.history):
            return self.history[-index]
        else:
            return ''

    def history_send(self):
        """
        Agrega una linea del historial para ser enviada
        """
        if self.history_cnt > len(self.history):
            self.history_cnt = len(self.history)
        elif self.history_cnt < 0:
            self.history_cnt = 0
        else:
            text = self.get_history(self.history_cnt)
            self.ventana.lineEditSend.setText(text)

    def closeEvent(self, event):
        """
        Cierra la aplicacion correctamente. Cerrar los puertos, detener thread
        y guardar la lista de comandos creada
        """
        if self.alive:
            self.close_port()
        file_cmd = open(self.commands_file, 'w')
        for line in self.commands_list:
            file_cmd.write(line + '\n')
        file_cmd.close()
        event.accept()

    def keyPressEvent(self, event):
        """
        Maneja eventos asociados a teclas presionadas
        """
        if event.key() == QtCore.Qt.Key_Up:
            if self.ventana.lineEditSend.hasFocus():
                self.history_cnt += 1
                self.history_send()

        if event.key() == QtCore.Qt.Key_Down:
            if self.ventana.lineEditSend.hasFocus():
                self.history_cnt -= 1
                self.history_send()

        event.accept()

    def configure_fuzz(self):
        """
        Abre la ventana para configurar número de comandos y secuencias a enviar
        """
        dialog = ConfigFuzzerDialog(self)
        fuzz, strategy, seqs, cmds, dir = dialog.run_tool()
        if fuzz:
            print("fuzzing")
            print(strategy, seqs, cmds, dir)

            self.fuzz_thread = threading.Thread(target=self.fuzz, args=(seqs, cmds, strategy, dir))
            self.fuzz_thread.daemon = True
            # self.fuzz_thread.setDaemon(True)
            self.fuzz_thread.start()

    def fuzz(self, seqs, cmds, strategy, dir):
        if type(seqs) is int and type(cmds) is int:
            input = fuzz(seqs, cmds, 0, 10, 33, 93, strategy, 'suchai_plantsat_cmd_list.csv')
            self.send_sequences(input, dir)
        self.fuzzingEnded.emit(self.unidentified_fuzz_errors, self.crashes_fuzz_errors, dir)

class EditCommandDialog(QtWidgets.QDialog):
    """
    Herramienta para edicion de comandos
    """

    def __init__(self, parent=None, cmd_list=None):

        QtWidgets.QDialog.__init__(self, parent)

        self.ventana = Ui_DialogEditCommandList()
        self.ventana.setupUi(self)

        self.cmd_list = cmd_list if cmd_list is not None else []
        for cmd in self.cmd_list:
            self.ventana.plainTextEditCommand.appendPlainText(cmd)

        # Conexiones
        self.ventana.pushButtonAdd.clicked.connect(self.add_item)

    def run_tool(self):
        """
        Abre el dialogo para que el usuario la lista. Al cerrar, recupera
        los cambios y retorna la nueva lista
        """
        ret = super(EditCommandDialog, self).exec_()
        if ret:
            self.cmd_list = self.ventana.plainTextEditCommand.toPlainText().split('\n')

        return self.cmd_list

    def delete_item(self):
        """
        Borra los item seleccionados
        """
        pass

    def add_item(self):
        """
        Agrega un nuevo item 
        """
        cmd = self.ventana.lineEditAdd.text()
        self.ventana.plainTextEditCommand.appendPlainText(cmd)

class ConfigFuzzerDialog(QtWidgets.QDialog):
    """
    Herramienta para edicion de comandos
    """

    def __init__(self, parent=None):

        QtWidgets.QDialog.__init__(self, parent)

        self.ventana = Ui_Dialog()
        self.ventana.setupUi(self)
        self.fuzz = False
        self.number_of_sequences = 0
        self.number_of_commands = 0
        self.strategy = RandomSequenceFuzzerWithFixedParamsAndExactTypes
        #self.ventana.pushFuzz.setEnabled(True)

    def run_tool(self):
        """
        Abre el dialogo para que el usuario la lista. Al cerrar, recupera
        los cambios y retorna la nueva lista
        """
        ret = super(ConfigFuzzerDialog, self).exec_()
        if (ret):
            if self.ventana.radioButtonStrat1.isChecked():
                self.strategy = RandomSequenceFuzzer
            elif self.ventana.radioButtonStrat2.isChecked():
                self.strategy = RandomSequenceFuzzerWithFixedParams
            else:
                self.strategy = RandomSequenceFuzzerWithFixedParamsAndExactTypes

            self.number_of_sequences = self.ventana.lineEditIter.text()
            self.number_of_commands = self.ventana.lineEditCmds.text()
            self.fuzz = True
            try:
                self.number_of_sequences = int(self.number_of_sequences)
                self.number_of_commands = int(self.number_of_commands)
            except ValueError:
                pass

        return self.fuzz, self.strategy, self.number_of_sequences, self.number_of_commands, self.ventana.logsDirectoryText.text()

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    gui = SerialCommander()
    gui.show()
    sys.exit(app.exec_())
