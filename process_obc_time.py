import os
import json
import re

experiment_path = "experiments_data/suchai3_obc_exp2_8_3/original/"
folders = os.listdir(experiment_path)

for folder in folders:
    if folder[0:6] == "strat_":
        logs_path = experiment_path + folder + "/logs/"
        sent_seqs_path = experiment_path + folder + "/sent_sequences/"
        log_files = os.listdir(logs_path)
        for log in log_files:
            seq_time = []
            with open(logs_path + log, "r") as logfile:
                time_pattern = "^\[RES  \]\[[0-9]{1,10}\]\[repoData\] [0-9]{1,10}"
                log_str = logfile.read()
            log_list = log_str.split('\n')
            for line in log_list:
                if re.fullmatch(time_pattern, line):
                    print(line)
                    seq_time.append(int(line.split("] ")[-1]))

            # Process time
            if len(seq_time) >= 2:
                obc_start_time = seq_time[0]
                obc_end_time = seq_time[-1]
                obc_exec_time = obc_end_time - obc_start_time

                # Add obc time to JSON
                with open(sent_seqs_path + log, "r") as json_file:
                    seq_dic = json.load(json_file)
                with open(sent_seqs_path + log, "w") as json_file:
                    seq_dic["obc_exec_time"] = obc_exec_time
                    json.dump(seq_dic, json_file, indent=2, separators=(',', ': '))