import os
from distutils.core import setup

version = "0.0.0"
with open(os.path.join("config", "version.txt")) as ver_file:
    version = ver_file.readline()[:-1]

setup(
    name='serialcommander',
    version=version,
    url='https://gitlab.com/carlgonz/SerialCommander',
    license='GPL v3',
    author='Carlos Gonzalez',
    author_email='carlgonz@uchile.cl',
    description='A serial interface to command embedded systems',
    
    packages=['forms'],
    py_modules=['SerialCommander'],
    scripts=['SerialCommander.py', ],
    include_package_data=True,
    install_requires=["pyserial"],
    data_files=[('share/serialcommander', ['config/config.json']),
                ('share/serialcommander', ['config/version.txt']),
                ('share/serialcommander', ['config/cmd_list.txt']),
                ('share/serialcommander', ['forms/icon.png']),
                ('share/applications', ['org.serialcommander.desktop'])]
)
